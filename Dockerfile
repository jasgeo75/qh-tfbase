FROM ubuntu:focal

ENV TERRAFORM_VERSION=0.13.3 \
    TERRAGRUNT_VERSION=v0.23.30

RUN set -eux; \
  apt-get update \
  && apt-get install -y \
    bash \
    curl \
    git \
    jq \
    wget \
    openssh-client \
    unzip \
  ;

#=============================================================================#
#    Add Github to known_hosts
#=============================================================================#
RUN mkdir -m 700 /root/.ssh; \
  touch -m 600 /root/.ssh/known_hosts; \
  ssh-keyscan github.com > /root/.ssh/known_hosts


#=============================================================================#
#    Install Terraform
#=============================================================================#
RUN set -eux; \
curl -sLO https://releases.hashicorp.com/terraform/${TERRAFORM_VERSION}/terraform_${TERRAFORM_VERSION}_linux_amd64.zip; \
  unzip terraform_${TERRAFORM_VERSION}_linux_amd64.zip -d /usr/local/bin; \
  rm -f terraform_${TERRAFORM_VERSION}_linux_amd64.zip

#=============================================================================#
#    Install Terragrunt
#=============================================================================#
RUN set -eux; \
  curl -L https://github.com/gruntwork-io/terragrunt/releases/download/${TERRAGRUNT_VERSION}/terragrunt_linux_amd64 \
  -o /usr/local/bin/terragrunt; \
  chmod +x /usr/local/bin/terragrunt; \
  terragrunt --version

#=============================================================================#
#    Install kubectl
#=============================================================================#
RUN set -eux; \
  KUBECTL_VERSION="$(curl -fsSL https://storage.googleapis.com/kubernetes-release/release/stable.txt)"; \
  curl -fsSLo /usr/local/bin/kubectl \
  "https://storage.googleapis.com/kubernetes-release/release/${KUBECTL_VERSION}/bin/linux/amd64/kubectl"; \
  chmod +x /usr/local/bin/kubectl; \
  kubectl version --client

#=============================================================================#
#    Install Helm 3.x as "helm3"
#=============================================================================#
RUN set -eux; \
  HELM3_VERSION="$(curl -fsSL https://api.github.com/repos/helm/helm/releases | \
    jq -r '[.[]|select(.prerelease == false)|.tag_name|select(test("^v3\\."))][0]')"; \
  curl -fsSLo helm.tar.gz \
  "https://get.helm.sh/helm-${HELM3_VERSION}-linux-amd64.tar.gz"; \
  tar xzf helm.tar.gz --strip-components=1 linux-amd64/helm; rm helm.tar.gz; \
  mv helm /usr/local/bin/helm3; \
  helm3 version



WORKDIR /opt/qh

ENTRYPOINT []

# http://label-schema.org/rc1/
LABEL org.label-schema.name="queryhub-install" \
      org.label-schema.description="QueryHub for AWS" \
      org.label-schema.vendor="Fincore" \
      org.label-schema.schema-version="1.0"

# vim: ts=2 sw=2 et sts=2 ft=dockerfile
